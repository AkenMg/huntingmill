package com.gasynet.huntingmill.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gasynet.huntingmill.entity.AppUser;
import com.gasynet.huntingmill.model.form.AppUserForm;
import com.gasynet.huntingmill.service.AppUserService;


@Controller
@RequestMapping("users")
public class AppUserController {
	
	Logger logger = LoggerFactory.getLogger(AppUserController.class);
	
	@Autowired
	AppUserService userService;
	
	@GetMapping("")
	public String list(Model model) {
		logger.info("access users page");
		model.addAttribute("users", userService.findAll());
		return "app/user/list";
	}
	
	@GetMapping("/create")
	public String create(Model model) {
		logger.info("access create use page");
		model.addAttribute("userForm", new AppUserForm());
		return "app/user/create";
	}
	
	@PostMapping("/create")
	public String createSubmit(Model model, @ModelAttribute("userForm") AppUserForm userForm) {
		logger.info("submit user form");
		System.out.println("user = " + userForm.toString());
		userService.save(userForm.toEntity());
		return "redirect:/users";
	}
	
	@GetMapping("/{id}")
	public String details(Model model, @PathVariable("id") Long id) {
		logger.info("access user details page");
		AppUser user = null;
		user = userService.find(id);
		model.addAttribute("user", user);
		System.out.println("user.getRoles() = "+user.getRoles());
		return "app/user/details";
	}
	
}
