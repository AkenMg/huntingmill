package com.gasynet.huntingmill.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("authentication")
public class AuthenticationController {
		
	Logger logger = LoggerFactory.getLogger(AuthenticationController.class);
	
	@GetMapping("/login")
	public String publicPage() {
		logger.info("access login page");
		return "security/login";
	}
}
