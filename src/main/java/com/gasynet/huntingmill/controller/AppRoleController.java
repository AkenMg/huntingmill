package com.gasynet.huntingmill.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gasynet.huntingmill.entity.AppRole;
import com.gasynet.huntingmill.entity.AppUser;
import com.gasynet.huntingmill.service.AppRoleService;

@Controller
@RequestMapping("roles")
public class AppRoleController {
Logger logger = LoggerFactory.getLogger(AppRoleController.class);
	
	@Autowired
	AppRoleService roleService;
	
	@GetMapping("")
	public String list(Model model) {
		logger.info("access roles page");
		model.addAttribute("roles", roleService.findAll());
		return "app/role/list";
	}
	
	@GetMapping("/{id}")
	public String details(Model model, @PathVariable("id") Long id) {
		logger.info("access role details page");
		AppRole role = null;
		role = roleService.find(id);
		model.addAttribute("role", role);
		return "app/role/details";
	}
}
