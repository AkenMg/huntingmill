package com.gasynet.huntingmill.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("security/")
public class AuthorizationController {
	
	Logger logger = LoggerFactory.getLogger(AuthorizationController.class);
	
	@GetMapping("public")
	public String publicPage() {
		logger.info("access public page");
		return "security/public";
	}
	
	@GetMapping("private")
	public String privatePage() {
		logger.info("access private page");
		return "security/private";
	}
	
	@GetMapping("private/regular")
	public String regularPage() {
		logger.info("access regular page");
		return "security/regular";
	}
	
	@GetMapping("private/admin")
	public String adminPage() {
		logger.info("access admin page");
		return "security/admin";
	}
	
	@GetMapping("own")
	public String ownPage() {
		logger.info("access own page");
		return "security/own";
	}
	
	@GetMapping("access-denied")
	public String accessDenied() {
		logger.info("access denied");
		return "security/access_denied";
	}
}
