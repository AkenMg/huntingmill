package com.gasynet.huntingmill.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SecurityService {
	
	@Autowired
	PasswordEncoder encoder;
	
	public String encodePassword(String password) {
		return encoder.encode(password);
	}
}
