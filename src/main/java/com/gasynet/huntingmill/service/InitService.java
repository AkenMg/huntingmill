package com.gasynet.huntingmill.service;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.gasynet.huntingmill.entity.AppRole;
import com.gasynet.huntingmill.entity.AppUser;


@Service
public class InitService {

	Logger logger = LoggerFactory.getLogger(InitService.class);
	
	@Autowired
	AppUserService userService;

	@Autowired
	AppRoleService roleService;
	
	@EventListener
    public void onApplicationEvent(ApplicationReadyEvent event) { 
		logger.debug("----APPLICATION INITIALIZATION START----");
		AppUser adminUser = new AppUser("admin","admin@gmail.com","admin");
		AppUser jhon = new AppUser("jon","jhon@gmail.com","jon");
		AppUser tiger = new AppUser("tiger","tiger@gmail.com","tiger");
		userService.register(adminUser);
		userService.register(jhon);
		userService.register(tiger);
		
		AppRole regular = new AppRole("REGULAR");
		AppRole admin = new AppRole("ADMIN");
		roleService.save(regular);
		roleService.save(admin);
		roleService.saveAll(Arrays.asList(regular,admin));

		adminUser.addRole(admin);
		adminUser.addRole(regular);
		jhon.addRole(regular);
		tiger.addRole(regular);
		
		userService.saveAll(Arrays.asList(adminUser,jhon,tiger));
		
		logger.debug("----APPLICATION INITIALIZATION END----");
	}
}
