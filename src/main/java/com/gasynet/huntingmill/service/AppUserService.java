package com.gasynet.huntingmill.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gasynet.huntingmill.entity.AppUser;
import com.gasynet.huntingmill.repository.AppUserRepository;


@Service
public class AppUserService {

	Logger logger = LoggerFactory.getLogger(AppUserService.class);
	
	@Autowired
	AppUserRepository repo;
	
	@Autowired
	SecurityService securityService;
	
	public List<AppUser> findAll(){
		return repo.findAll();
	}
	
	public AppUser find(Long id) {
		return repo.findById(id).get();
	}
	
	public void save(AppUser entity) {
		repo.save(entity);
	}
	
	public void saveAll(List<AppUser> list) {
		repo.saveAll(list);
	}
	
	public void delete(AppUser entity) {
		repo.delete(entity);
	}
	
	public void delete(Long id) {
		repo.deleteById(id);
	}
	
//	SPECIFIC REQUESTS
	
	public AppUser findByUsername(String username) {
		return repo.findByUsername(username);
	}
	
	public void register(AppUser entity) {
		String encodedPassword = securityService.encodePassword(entity.getUserPassword());
		entity.setUserPassword(encodedPassword);
		repo.save(entity);
	}
}
