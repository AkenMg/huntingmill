package com.gasynet.huntingmill.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.gasynet.huntingmill.entity.AppRole;
import com.gasynet.huntingmill.repository.AppRoleRepository;

@Service
public class AppRoleService {
	@Autowired
	AppRoleRepository repo;
	
	public List<AppRole> findAll(){
		return repo.findAll();
	}
	
	public AppRole find(Long id) {
		return repo.findById(id).get();
	}
	
	public void save(AppRole entity) {
		repo.save(entity);
	}
	
	public void saveAll(List<AppRole> list) {
		repo.saveAll(list);
	}
	
	public void delete(AppRole entity) {
		repo.delete(entity);
	}
	
	public void delete(Long id) {
		repo.deleteById(id);
	}
}
