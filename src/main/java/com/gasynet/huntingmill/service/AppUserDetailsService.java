package com.gasynet.huntingmill.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.gasynet.huntingmill.entity.AppUser;
import com.gasynet.huntingmill.model.AppUserDetails;

@Service
public class AppUserDetailsService implements UserDetailsService{

	
	Logger logger = LoggerFactory.getLogger(AppUserDetailsService.class);
	
	@Autowired
	AppUserService userService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUser user = userService.findByUsername(username);
		if(user==null){
            throw new UsernameNotFoundException(username); 
        }
        logger.debug("Logging User: " + user.getUsername());
        return new AppUserDetails(user);
	}

}
