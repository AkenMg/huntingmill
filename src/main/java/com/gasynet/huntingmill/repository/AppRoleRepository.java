package com.gasynet.huntingmill.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gasynet.huntingmill.entity.AppRole;

public interface AppRoleRepository extends JpaRepository<AppRole,Long>{
	
}
