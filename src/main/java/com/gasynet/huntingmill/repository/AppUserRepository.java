package com.gasynet.huntingmill.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gasynet.huntingmill.entity.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser,Long> {
	AppUser findByUsername(String username);
}
