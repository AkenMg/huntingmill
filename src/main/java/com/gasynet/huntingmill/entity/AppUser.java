package com.gasynet.huntingmill.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

@Entity
public class AppUser extends BaseEntity{
	
	private String username;
	private String email;
	private String userPassword;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<AppRole> roles = new HashSet<>();
	
	
//	CONSTRUCTORS
	
	public AppUser() {
	}
	
	public AppUser(String username, String email, String userPassword) {
		super();
		this.username = username;
		this.email = email;
		this.userPassword = userPassword;
	}
	
//	GETTERS SETTERS
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public Set<AppRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<AppRole> roles) {
		this.roles = roles;
	}
	
//	ASSOCIATIONS METHODS
	public void addRole(AppRole newRole) {
		this.roles.add(newRole);
		newRole.getUsers().add(this);
	}
	
	public void removeRole(AppRole removedRole) {
		this.roles.remove(removedRole);
		removedRole.getUsers().remove(this);
	}
}
