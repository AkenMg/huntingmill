package com.gasynet.huntingmill.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class AppRole extends BaseEntity{
	
	private String roleName;
	
	@ManyToMany(mappedBy = "roles")
	private Set<AppUser> users = new HashSet<>();
	
//	CONSTRUCTORS
	
	public AppRole() {
		// TODO Auto-generated constructor stub
	}
	
	public AppRole(String roleName) {
		super();
		this.roleName = roleName;
	}

//	GETTERS SETTERS
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Set<AppUser> getUsers() {
		return users;
	}

	public void setUsers(Set<AppUser> users) {
		this.users = users;
	}
	
//	ASSOCIATION METHODS
	public void addUser(AppUser newUser) {
		this.users.add(newUser);
		newUser.getRoles().add(this);
	}
	
	public void removeUser(AppUser removedUser) {
		this.users.remove(removedUser);
		removedUser.getRoles().remove(this);
	}

	@Override
	public String toString() {
		return "[id=" + this.getId() + ", roleName=" + roleName + "]";
	}
	

}
