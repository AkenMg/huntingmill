package com.gasynet.huntingmill.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.gasynet.huntingmill.service.AppUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	AppUserDetailsService detailService;
	
	@Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }
    
    @Bean
    public DaoAuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(detailService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }
    
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http
            .authorizeRequests()
	            .antMatchers("/security/private/regular").hasAuthority("REGULAR")
	            .antMatchers("/security/private/admin").hasAuthority("ADMIN")
            	.antMatchers("/security/private**").authenticated()
                .anyRequest().permitAll()
                .and()
            .formLogin()
                .loginPage("/authentication/login")
            	.loginProcessingUrl("/authentication/login")
                .permitAll()
                .and()
            .logout()
            	.logoutUrl("/authentication/logout")
            	.logoutSuccessUrl("/")
                .permitAll()
                .and()
            .exceptionHandling().accessDeniedPage("/security/access-denied");
                        
    }
	
}
