package com.gasynet.huntingmill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HuntingMillApplication {

	public static void main(String[] args) {
		SpringApplication.run(HuntingMillApplication.class, args);
	}

}
