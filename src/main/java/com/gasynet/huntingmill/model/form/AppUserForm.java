package com.gasynet.huntingmill.model.form;

import com.gasynet.huntingmill.entity.AppUser;

public class AppUserForm {
	
	private String username;
	private String email;
	private String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public AppUser toEntity() {
		AppUser entity = new AppUser();
		entity.setEmail(this.email);
		entity.setUsername(this.username);
		entity.setUserPassword(this.password);
		return entity;
	}
	
	@Override
	public String toString() {
		return "AppUserForm [username=" + username + ", email=" + email + ", password=" + password + "]";
	}
	
}
